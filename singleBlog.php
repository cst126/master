<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        8/2017
 *
 * File:        singleBlog.php
 *
 * Displays a single blog post with options to update or delete
 * when it is your own post.
 */

require_once "includes/pageLoader.php";

$pageVars = [];

// Verify if the user is logged in.
$verifyLogin = new \cst126\verifyLogin();

// Check if post was submitted to be deleted
$delete = new \cst126\deletePost();
$delete->delete();

// Check if post was submitted to add comment to post
$comment = new \cst126\AddComment();
$comment->doAddComment();


if(!$_GET) {
	$_SESSION["warningMsg"] = 'Click <a href="blogList.php">here</a> to find a blog entry';
} elseif ( ! isset($_GET["delete"])) {
	$db   = new \cst126\Database();
	$conn = $db->connection();

	$blogId = $_GET["id"];

	$query = $conn->query( "SELECT blogEntry.id, blogEntry.title, blogEntry.content, blogEntry.created_at, userBlog.userId
		    FROM blogEntry 
		    JOIN userBlog
		    ON userBlog.blogId = blogEntry.id
		    WHERE blogEntry.id = $blogId
		    ORDER BY blogEntry.created_at DESC LIMIT 10;" );

	$blog = $query->fetch_assoc();

	if ( $blog ) {
		$pageVars["blogPost"] = [
			'id'            =>  $blog["id"],
			'title'         =>  $blog["title"],
			'created_at'    =>  date('m/d/Y', strtotime($blog["created_at"])),
			'content'       =>  html_entity_decode( nl2br($blog["content"]) ),
			'userId'        =>  $blog["userId"],
            "access"        =>  $_SESSION["access"]
		];
	}
}

// Check session vars
$chkSession = new cst126\CheckSession();
$pageVars = array_merge($pageVars, $chkSession->sessionResults);

//var_dump($pageVars);

if ($verifyLogin->verify()) {
	// Check to see if post was just deleted. If so, load blogList instead
	if (isset($_GET["delete"])) {
		$pageVars["pageTitle"] = "CST 126 Blog Application";
		$pageVars["pageName"]  = "index";
		$pageVars["access"] = $_SESSION["access"];

		echo $twig->render( 'index.html.twig', $pageVars );
	} else {
		$pageVars["pageTitle"] = "CST 126 Blog Application | Blog List";
		$pageVars["pageName"]  = "singleBlog";
		$pageVars["access"]    = $_SESSION["access"];

		$getComments          = new cst126\getComments();
		$getComments          = $getComments->findComments();
		$pageVars["comments"] = $getComments;

		echo $twig->render( 'singleBlog.html.twig', $pageVars );
	}
} else {
	$pageVars["pageTitle"] = "Login | CST-126 Blog";
	$pageVars["pageName"] = "login";

	echo $twig->render( 'login.html.twig', $pageVars );
}