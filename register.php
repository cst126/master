<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        8/2017
 *
 * File:        register.php
 *
 * This is the registration page for the blog.
 */

require_once "includes/pageLoader.php";

$pageVars = [];

// Verify if the user is logged in.
$verifyLogin = new \cst126\verifyLogin();


$registerResult = false;

// Check if form submitted
if (isset($_POST["submit"])) {
	$registerSubmit = new \cst126\RegisterSubmit();
	$registerResult = $registerSubmit->doRegister();
}


// Check session vars
$chkSession = new cst126\CheckSession();
$pageVars = array_merge($pageVars, $chkSession->sessionResults);

//var_dump($pageVars);

if ($registerResult) {
	$pageVars["pageTitle"] = "Login | CST-126 Blog";
	$pageVars["pageName"] = "login";

	echo $twig->render( 'login.html.twig',  $pageVars );
} else {
	$pageVars["pageTitle"] = "Registration | CST-126 Blog";
	$pageVars["pageName"]  = "register";

	echo $twig->render( 'register.html.twig', $pageVars );
}