<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        7/2017
 *
 * File:        RegisterSubmit.php
 *
 * Handles new user registration
 */

namespace cst126;

use cst126\Database;

class RegisterSubmit
{
	public function doRegister() {
		// Set all submitted values into session
		foreach ($_POST as $key => $val) {
			$_SESSION[$key] = $_POST[$key];
		}
		unset($key); unset($val);

		// Start validation
		if ($_POST['fName'] == "" || $_POST['lName'] == "" || $_POST['username'] == "" || $_POST['email'] == "" ||
		    $_POST['pass'] == "" || $_POST['passConfirm'] == "") {
			$_SESSION['errMsg'] = "Please fill in all values";
			return false;
		}

		// Validate email
		if ( ! filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			$_SESSION['errMsg'] = "Please enter a valid email address";
			return false;
		}

		// Validate passwords match
		if ($_POST['pass'] != $_POST['passConfirm']) {
			$_SESSION['errMsg'] = "Passwords did not match";
			return false;
		}

		// Check if username or email already exists in database
		$db = new \cst126\Database();

		if ($conn = $db->connection()) {
			$result = $conn->query("SELECT COUNT(*) FROM users WHERE username = '".$_POST['username']."' OR email = '".$_POST['email']."';");
			$row = $result->fetch_row();

			if ($row[0] <= 0) {
				$date = date('Y-m-d H:i:s', time());

				$insert = $conn->prepare("INSERT INTO users (fName, lName, username, email, password, created_at, access) VALUES (?, ?, ?, ?, ?, '$date', 'user');");

				if ($insert) {
					// Setup hashed pw
					$hashPw = $db->hashPw($_POST['pass']);

					$firstName = preg_replace('/\s+/', ' ', $_POST['fName']);
					$lastName = preg_replace('/\s+/', ' ', $_POST['lName']);

					$insert->bind_param('sssss', $firstName, $lastName, $_POST['username'], $_POST['email'], $hashPw);

					if ($insert->execute()) {
						$_SESSION['successMsg'] = "Thank you for registering! Please log in";

						$conn->close();
						return true;
					} else {
						$_SESSION['errMsg'] = $insert->error;

						$conn->close();
						return false;
					}
				} else
					$_SESSION['errMsg'] = $conn->error;

				$conn->close();
				return false;
			} else {
				$_SESSION['errMsg'] = "Username or email already exists";

				$conn->close();
				return false;
			}
		} else {
			$_SESSION['errMsg'] = $conn->error;

			$conn->close();
			return false;
		}
	}
}