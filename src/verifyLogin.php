<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        7/2017
 *
 * File:        verifyLogin.php
 *
 * This class will verify if a user is logged in and return true or false
 */

namespace cst126;

use mysqli;
use cst126\Database;

class verifyLogin
{
    /**
     * verify login for each page
     */
    public function verify()
    {
        $db = new Database();

        // Get Server URL
        $host  = $_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);

        // If SESSION username and pass are not set re-direct to login page
        if(!isset($_SESSION["usernameLoggedIn"]) ) {
	        $_SESSION['warningMsg'] = "Please login before continuing";
            return false;
        }
        else {
            // Validate username
            if ($conn = $db->connection()) {
                $result = $conn->query("SELECT * FROM users WHERE username = '" . $_SESSION["usernameLoggedIn"] . "';");
                $row = $result->fetch_row();
                // If there is no username in database
                if ($row[0] <= 0) {
                    $_SESSION['errMsg'] = "Invalid Username or Password";
	                return false;

                } else {
                    // If password does not match hash password from database
                    if (!password_verify($_SESSION['pass'], $row[5])) {
	                    return false;
                    }
                }

            }
        }

		return true;
    }
}