<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        8/2017
 *
 * File:        UpdateBlogEntry.php
 *
 * Process an update to a blog entry
 */

namespace cst126;

use cst126\Database;

class UpdateBlogEntry
{
	public function doBlogUpdate() {
		// Set all submitted values into session
		foreach ($_POST as $key => $val) {
			$_SESSION[$key] = $_POST[$key];
		}
		unset($key); unset($val);

		// Start validation
		if (trim($_POST['title']) == "" || trim($_POST['textEntry']) == "") {
			$_SESSION['errMsg'] = "Please fill in all values";
			return false;
		}

		// Make sure the title only contains letter, numbers, and spaces
		if ( ! ctype_alnum( str_replace(' ', '', $_POST['title']) )) {
			$_SESSION['errMsg'] = "Title can only contain letters, numbers, and spaces";
			return false;
		}

		$db = new \cst126\Database();

		if ($conn = $db->connection()) {
			$blogTitle = trim($_POST['title']);
			$blogEntry = htmlspecialchars( trim($_POST['textEntry']) );

			$insert = $conn->prepare("UPDATE blogEntry SET title = ?, content = ? WHERE id = ".$_POST["blogId"].";");
			$insert->bind_param('ss', $blogTitle,  $blogEntry);

			if ($insert->execute()) {
				// Clear session variables and add success message
				unset($_SESSION['title']);
				unset($_SESSION['textEntry']);
				$_SESSION['successMsg'] = "Blog Post Updated";
				$conn->close();
				return true;
			} else {
				$_SESSION['errMsg'] = $insert->error;
				$conn->close();
				return false;
			}
		} else {
			$_SESSION['errMsg'] = $conn->error;
			return false;
		}
	}
}