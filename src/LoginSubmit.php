<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        7/2017
 *
 * File:        LoginSubmit.php
 *
 * Handles the login form
 */

namespace cst126;

use cst126\Database;

class LoginSubmit
{

	/**
	 * Validates the login and returns a true or false resolution
	 * based upon the submitted data
	 *
	 * @return bool
	 */
	public function validateLogin() {
		$db = new Database();

		// Set all submitted values into session
		foreach ($_POST as $key => $val) {
			$_SESSION[$key] = $_POST[$key];
		}
		unset($key); unset($val);

		// Start validation
		if ($_POST['username'] == "" || $_POST['pass'] == "") {
			$_SESSION['errMsg'] = "Please fill in all values";
			return false;
		}

		// Set row variable for query results
		$row = [];

		// Validate username
		if ($conn = $db->connection()) {
			$result = $conn->query("SELECT * FROM users WHERE username = '".preg_replace('/\s+/', ' ', $_POST['username'])."';");
			$row = $result->fetch_row();

			if ($row[0] <= 0) {
				$_SESSION['errMsg'] = "Invalid Username or Password";
				return false;
			} else {
				if ( ! password_verify($_POST['pass'], $row[5])) {
					$_SESSION['errMsg'] = 'Invalid password.';
					return false;
				}
			}
		}

		// Everything was good. Set logged in session variables
		$_SESSION["usernameLoggedIn"] = $_POST['username'];
		$_SESSION["userIdLoggedIn"] = $row[0];
		$_SESSION["access"] = $row[6];

		return true;
	}
}