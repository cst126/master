<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        8/2017
 *
 * File:        Messages.php
 *
 * This class will manage errors
 */

namespace cst126;

class CheckSession
{
	public $sessionResults = [];

	/**
	 * CheckSession constructor
	 *
	 * Checks defined session variables and returns array of results
	 * so they can be viewed or used within the templates
	 */
	public function __construct() {
		if (isset($_SESSION["errMsg"])) {
			$this->sessionResults["errMsg"] = $_SESSION["errMsg"];
			unset($_SESSION["errMsg"]);
		}

		if (isset($_SESSION["warningMsg"])) {
			$this->sessionResults["warningMsg"] = $_SESSION["warningMsg"];
			unset($_SESSION["warningMsg"]);
		}

		if (isset($_SESSION["successMsg"])) {
			$this->sessionResults["successMsg"] = $_SESSION["successMsg"];
			unset($_SESSION["successMsg"]);
		}

		if (isset($_SESSION["usernameLoggedIn"])) {
			$this->sessionResults["usernameLoggedIn"] = $_SESSION["usernameLoggedIn"];
		}

		if (isset($_SESSION["userIdLoggedIn"])) {
			$this->sessionResults["userIdLoggedIn"] = $_SESSION["userIdLoggedIn"];
		}

		if (isset($_SESSION["access"])) {
			$this->sessionResults["access"] = $_SESSION["access"];
		}

		return $this->sessionResults;
	}
}