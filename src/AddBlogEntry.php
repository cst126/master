<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        8/2017
 *
 * File:        AddBlogEntry.php
 *
 * Handles the adding of blog posts
 */

namespace cst126;

use cst126\Database;

class AddBlogEntry
{
	public function doBlogAdd() {
		// Set all submitted values into session
		foreach ($_POST as $key => $val) {
			$_SESSION[$key] = $_POST[$key];
		}
		unset($key); unset($val);

		// Start validation
		if (trim($_POST['title']) == "" || trim($_POST['textEntry']) == "") {
			$_SESSION['errMsg'] = "Please fill in all values";
			return false;
		}

		// Make sure the title only contains letter, numbers, and spaces
		if ( ! ctype_alnum( str_replace(' ', '', $_POST['title']) )) {
			$_SESSION['errMsg'] = "Title can only contain letters, numbers, and spaces";
			return false;
		}

		$db = new \cst126\Database();

		if ($conn = $db->connection()) {
			$blogTitle = trim($_POST['title']);
			$blogEntry = htmlspecialchars( trim($_POST['textEntry']) );

			$insert = $conn->prepare("INSERT INTO blogEntry (title, content) VALUES (?, ?);");
			$insert->bind_param('ss', $blogTitle,  $blogEntry);

			if ($insert->execute()) {
				// Now we need to get the newly added blog id and add to the userBlog table
				$lastBlogId = mysqli_insert_id($conn);

				if ($lastBlogId) {
					$insert = $conn->query("INSERT INTO userBlog VALUES (".$_SESSION["userIdLoggedIn"].", ".$lastBlogId.");");

					if ($insert) {
						// Clear session variables and add success message
						unset($_SESSION['title']);
						unset($_SESSION['textEntry']);
						$_SESSION['successMsg'] = "Blog Post Added";
						$conn->close();

						return true;
					}
				}
			} else {
				$_SESSION['errMsg'] = $insert->error;
				return false;
			}

			$conn->close();
		} else {
			$_SESSION['errMsg'] = $conn->error;
			return false;
		}
	}
}