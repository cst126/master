<?php
/**
 * Created by PhpStorm.
 * User: aaron.fonseca
 * Date: 8/30/17
 * Time: 3:03 PM
 */

namespace cst126;

use cst126\Database;

class getComments
{
	private $results = [];

    function findComments()
    {
        $db = new \cst126\Database();

        if ($conn = $db->connection()) {
            $blogId = $_GET["id"];
            $query = $conn->query( "SELECT com.id, com.blogId, com.comment, com.added, users.username
				FROM comments AS com
				JOIN userComments AS uc
				JOIN users
				ON com.id = uc.commentId
				WHERE blogId = '$blogId'
				AND users.id = uc.userId"
            );

	        if ($query) {
		        while ( $blog = $query->fetch_assoc() ) {
			        array_push($this->results, [
						"id"        =>  $blog['id'],
						"blogId"    =>  $blog['blogId'],
						"username"  =>  $blog["username"],
						"comment"   =>  nl2br($blog['comment']),
						"added"     =>  $blog['added']
			        ]);
		        }
	        }

            return $this->results;

        }
    }
}