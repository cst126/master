<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        7/2017
 *
 * File:        deletePost.php
 *
 * This class will house the default connection to the database
 */

namespace cst126;

use mysqli;
use cst126\Database;

class deletePost
{
    /**
     *
     * delete post
     */
    public function delete()
    {
        if(isset($_GET["delete"]))
        {
            $delete = $_GET["delete"];

            $db = new Database();
		    $conn = $db->connection();

		    try {
		    	// Try to remove the blog entry first
		    	$delQry = "DELETE blogEntry, userBlog
					FROM blogEntry 
					INNER JOIN userBlog 
					WHERE userBlog.blogId = blogEntry.id 
					AND blogEntry.id = '$delete';";

			    $query = $conn->query( $delQry );

			    // Now delete any comments associated with this post
			    $delQry2 = "DELETE comments, userComments
			        FROM comments 
					INNER JOIN userComments 
					WHERE comments.blogId = $delete 
					AND userComments.commentId = comments.id;";

			    $query2 = $conn->query( $delQry2 );

			    $_SESSION["successMsg"] = "Blog post removed";
		    } catch (\Exception $exc) {
		    	$_SESSION["errMsg"] = "There was a problem removing this post";
		    }
        }
    }
}