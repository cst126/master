<?php
/**
 * Created by PhpStorm.
 * User: aaron.fonseca
 * Date: 8/28/17
 * Time: 3:56 PM
 */

namespace cst126;

use cst126\Database;

class AddComment
{
    public function doAddComment() {
        if(!isset($_POST["comment"])){
            return false;
        };
        // Set all submitted values into session
        foreach ($_POST as $key => $val) {
            $_SESSION[$key] = $_POST[$key];
        }
        unset($key); unset($val);
        //var_dump($_POST);

        // Start validation
        if (trim($_POST['comment']) == "" || trim($_POST['comment']) == "") {
            $_SESSION['errMsg'] = "Please enter a comment";
            return false;
        }

        $db = new \cst126\Database();

        // Create DB connection
        if ($conn = $db->connection()) {
            $comment = htmlspecialchars( trim($_POST['comment']) );
            $blogId = $_GET["id"];
            $insert = $conn->prepare("INSERT INTO comments (blogId, comment) VALUES (?, ?)");
            $insert->bind_param('ss', $blogId, $comment);

            if ($insert->execute()) {
            	$lastInsertedCommentId = $insert->insert_id;

            	// Comment was added, so let's add the user reference
	            $insert2 = $conn->prepare("INSERT INTO userComments VALUES (?, ?)");
	            $insert2->bind_param('dd', $_SESSION["userIdLoggedIn"], $lastInsertedCommentId);

	            if ($insert2->execute()) {
		            // Clear session variables and add success message
		            $_SESSION['successMsg'] = "Blog Comment Added";
		            $conn->close();
		            return true;
	            } else {
		            $_SESSION['errMsg'] = $insert2->error;
		            $conn->close();
		            return false;
	            }
            } else {
                $_SESSION['errMsg'] = $insert->error;
                $conn->close();
                return false;
            }
        } else {
            $_SESSION['errMsg'] = $conn->error;
            return false;
        }
    }
}