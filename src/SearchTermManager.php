<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        8/2017
 *
 * File:        SearchtermManager.php
 *
 * Allows you to manage the search term processes. You can add,
 * remove, view, and edit search terms.
 */

namespace cst126;

use cst126\Database;

class SearchTermManager
{

	private $conn;

	/**
	 * SearchTermManager constructor
	 */
	public function __construct() {
		$dbConn = new \cst126\Database();
		$this->conn = $dbConn->connection();
	}

	/**
	 * Adds a search term to database. Will record the term, term count, and
	 * the user that requested the search term.
	 *
	 * @param $searchTerm
	 * @param $userId
	 *
	 * @return bool
	 */
	public function addSearchTerm($searchTerm, $userId) {
		if (self::checkTermExists($searchTerm)) {

			// Term exists, update term count
			$update = $this->conn->query("UPDATE searchTerms SET termCount = termCount + 1 
				WHERE term = '$searchTerm' LIMIT 1;");

			if ( ! $update) {
				$_SESSION["errMsg"] = $update->error;
				return false;
			}

			// Get the last updated ID
			$query = $this->conn->query("SELECT id FROM searchTerms WHERE term = '$searchTerm'");
			$result = $query->fetch_assoc();
			$updatedTermId = $result["id"];

			if ($insert = $this->conn->query("INSERT INTO searchTermByUser VALUES ($userId, $updatedTermId)")) {
				return true;
			} else {
				$_SESSION["errMsg"] = "Term was added but user record may not have been recorded";
				return false;
			}

		} else {

			// Term did not exist, so lets add the new term
			$insert = $this->conn->prepare("INSERT INTO searchTerms (termCount, term) VALUES (1, ?)");
			$insert->bind_param('s', $searchTerm);

			if ( ! $insert->execute()) {
				$_SESSION["errMsg"] = $insert->error;
				return false;
			}

			// Get the last inserted ID
			$lastInsertedId = mysqli_insert_id($this->conn);

			if ($insert = $this->conn->query("INSERT INTO searchTermByUser VALUES ($userId, $lastInsertedId)")) {
				return true;
			} else {
				$_SESSION["errMsg"] = "Term was added but user record may not have been recorded";
				return false;
			}

		}

	}

	/**
	 * Checks to see if the search term already exists in the database.
	 *
	 * @param $searchTerm
	 *
	 * @return bool
	 */
	private function checkTermExists($searchTerm) {
		$query = $this->conn->query("SELECT COUNT(*) FROM searchTerms WHERE term = '$searchTerm';");

		return $query->fetch_row()[0] == 0 ? false : true;
	}

}