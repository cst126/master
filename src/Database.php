<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        7/2017
 *
 * File:        Database.php
 *
 * This class will house the default connection to the database
 */

namespace cst126;

use mysqli;

class Database
{
	private $server = "localhost";
	private $user = "cst126_user";
	private $pw = "upc1234";
	private $db = "cst126_blog";

	// For rpearl credentials on Hostable
//	private $server = "localhost";
//	private $user = "rpearlbh_user";
//	private $pw = "upc1234";
//	private $db = "rpearlbh_cst126_blog";

//    private $servername = "localhost";
//    private $username = "afonseca_root";
//    private $password = "cst126";
//    private $database = "afonseca_cst126_blog";

	/**
	 * Creates connection to the database. Returns FALSE if connection fails
	 *
	 * @return bool|\mysqli
	 */
	public function connection() {
		$conn = new mysqli($this->server, $this->user, $this->pw, $this->db);

		if ($conn->connect_error)
			return FALSE;
		else
			return $conn;
	}

	/**
	 * Use the server to bcrypt the password. It can be later verified
	 * against using the password_verify().
	 *
	 * @param $pass
	 *
	 * @return bool|string
	 */
	public function hashPw($pass) {
		return password_hash($_POST['pass'], PASSWORD_BCRYPT, [
			'cost'  => 11
		]);
	}
}