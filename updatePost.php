<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        8/2017
 *
 * File:        updatePost.php
 *
 * Shows a current post and allows you to update it.
 */

require_once "includes/pageLoader.php";

$pageVars = [];

// Verify if the user is logged in.
$verifyLogin = new \cst126\verifyLogin();


$updateResult = false;

// Check if form submitted
if (isset($_POST["submit"])) {
	$updateSubmit = new \cst126\UpdateBlogEntry();
	$updateResult = $updateSubmit->doBlogUpdate();
}

if(!$_GET) {
	$_SESSION["warningMsg"] = 'Click <a href="blogList.php">here</a> to find a blog entry';
} else {
	$db = new \cst126\Database();
	$conn = $db->connection();

	$blogId = $_GET["id"];

	$query = $conn->query("SELECT blogEntry.id, blogEntry.title, blogEntry.content, userBlog.userId
    FROM blogEntry 
    JOIN userBlog
    ON userBlog.blogId = blogEntry.id
    WHERE blogEntry.id = $blogId;");

	$blog = $query->fetch_assoc();

	if ( $blog ) {

        $pageVars["blogPost"] = [
            'id'            =>  $blog["id"],
            'title'         =>  $blog["title"],
            'content'       =>  $blog["content"],
            'userId'        =>  $blog["userId"]
        ];

	}
}

// Check session vars
$chkSession = new cst126\CheckSession();
$pageVars = array_merge($pageVars, $chkSession->sessionResults);

//var_dump($pageVars);

if ($verifyLogin->verify()) {
	$pageVars["pageTitle"] = "CST 126 Blog Application | Blog List";
	$pageVars["pageName"] = "updateBlog";

	echo $twig->render( 'updateBlog.html.twig', $pageVars );
} else {
	$pageVars["pageTitle"] = "Login | CST-126 Blog";
	$pageVars["pageName"] = "login";

	echo $twig->render( 'login.html.twig', $pageVars );
}