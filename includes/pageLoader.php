<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        8/2017
 *
 * File:        pageLoader.php
 *
 * Load dependency classes and any other required applications, and
 * then start the site load.
 */

session_start();

require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader, array(
//	'cache' => 'cache',
));

if (ini_get('date.timezone') == '') {
	date_default_timezone_set('UTC');
}