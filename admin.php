<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        8/20/2017
 *
 * File:        admin.php
 *
 * This file is only accessible if you have admin rights.
 */

require_once "includes/pageLoader.php";

$pageVars = [];

// Verify if the user is logged in.
$verifyLogin = new \cst126\verifyLogin();


// Check session vars
$chkSession = new cst126\CheckSession();
$pageVars = array_merge($pageVars, $chkSession->sessionResults);

if (!isset($_SESSION["access"]) || $_SESSION["access"] !== "admin") {
	$pageVars["pageTitle"] = "Login | CST-126 Blog";
	$pageVars["pageName"] = "login";

	echo $twig->render( 'login.html.twig',  $pageVars );
	exit;
}

// Get most recent blog posts
$pageVars["blogPosts"] = [];

$db = new \cst126\Database();
$conn = $db->connection();

$result = $conn->query("SELECT * FROM users");

if ($result->num_rows > 0) {

    while ($blog = $result->fetch_assoc()) {

	    array_push($pageVars["blogPosts"], [
		    'fName'         =>  $blog["fName"],
		    'lName'         =>  $blog["lName"],
		    'username'      =>  $blog["username"],
		    'access'        =>  $blog["access"]
	    ]);

    }

}

$conn->close();


$pageVars["pageTitle"] = "Admin | CST-126 Blog";
$pageVars["pageName"] = "admin";

echo $twig->render( 'admin.html.twig',  $pageVars );