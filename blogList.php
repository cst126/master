<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        7/2017
 *
 * File:        blogList.php
 *
 * Lists out each blog entry and allows you to view its details.
 */

require_once "includes/pageLoader.php";

$pageVars = [];

// Verify if the user is logged in.
$verifyLogin = new \cst126\verifyLogin();

// Get most recent blog posts
$pageVars["blogPosts"] = [];

$db = new \cst126\Database();
$conn = $db->connection();

// Check if we are on this page due to a search, or if we are
// just listing out all blog posts.
if (isset($_POST["search_submit"]) && trim($_POST["search_term"]) != "") {

	// List blogs based on search results
	$searchTerm = trim( $_POST["search_term"] );

	// Check and add the search term to DB
	$stm = new \cst126\SearchTermManager();
	$stm->addSearchTerm($searchTerm, $_SESSION["userIdLoggedIn"]);

	$query = $conn->query("SELECT blogEntry.id, blogEntry.title, blogEntry.content, blogEntry.created_at, users.username FROM blogEntry
    JOIN userBlog
    ON blogEntry.id = userBlog.blogId
    JOIN users
    ON userBlog.userId = users.id
    WHERE blogEntry.title LIKE '%$searchTerm%'
    OR blogEntry.content LIKE '%$searchTerm%'
    OR users.username LIKE '%$searchTerm%'
    ORDER BY blogEntry.created_at DESC LIMIT 20;");

	if ($query) {
		while ( $blog = $query->fetch_assoc() ) {
			array_push($pageVars["blogPosts"], [
				'id'            =>  $blog["id"],
				'title'         =>  html_entity_decode( $blog["title"] ),
				'created_at'    =>  date('m/d/Y', strtotime($blog["created_at"])),
				'content'       =>  html_entity_decode( $blog["content"] ),
				'username'      =>  $blog["username"]
			]);
		}
	}
} else {
	// List all blogs
	$query = $conn->query("SELECT blogEntry.id, blogEntry.title, blogEntry.content, blogEntry.created_at, users.username FROM blogEntry
    JOIN userBlog
    ON blogEntry.id = userBlog.blogId
    JOIN users
    ON userBlog.userId = users.id
    ORDER BY blogEntry.created_at DESC LIMIT 20;");

	if ($query) {
		while ( $blog = $query->fetch_assoc() ) {
			array_push($pageVars["blogPosts"], [
				'id'            =>  $blog["id"],
				'title'         =>  html_entity_decode( $blog["title"] ),
				'created_at'    =>  date('m/d/Y', strtotime($blog["created_at"])),
				'content'       =>  html_entity_decode( $blog["content"] ),
				'username'      =>  $blog["username"]
			]);
		}
	}
}

//var_dump($pageVars);


// Check session vars
$chkSession = new cst126\CheckSession();
$pageVars = array_merge($pageVars, $chkSession->sessionResults);


if ($verifyLogin->verify()) {
	$pageVars["pageTitle"] = "Blog List | CST-126 Blog";
	$pageVars["pageName"] = "blogList";

	echo $twig->render( 'blogList.html.twig', $pageVars );
} else {
	$pageVars["pageTitle"] = "Login | CST-126 Blog";
	$pageVars["pageName"] = "login";

	echo $twig->render( 'login.html.twig', $pageVars );
}