<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        7/2017
 *
 * File:        login.php
 *
 * This is the login page for the blog.
 */

require_once "includes/pageLoader.php";

$pageVars = [];
$loginResult = false;

// Check if form submitted
if (isset($_POST["submit"])) {
    $loginSubmit = new cst126\LoginSubmit();
    $loginResult = $loginSubmit->validateLogin();
}

// Check session vars
$chkSession = new cst126\CheckSession();
$pageVars = array_merge($pageVars, $chkSession->sessionResults);

//var_dump($pageVars);

// If loginResult is true, load homepage template
if ($loginResult) {
	$pageVars["pageTitle"] = "CST 126 Blog Application";
	$pageVars["pageName"] = "index";

	echo $twig->render( 'index.html.twig', $pageVars );
} else {
	$pageVars["pageTitle"] = "Login | CST-126 Blog";
	$pageVars["pageName"] = "login";

	echo $twig->render( 'login.html.twig',  $pageVars );
}