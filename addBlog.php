<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        8/2017
 *
 * File:        addBlog.php
 *
 * Once logged in, this page allows you to add a blog entry
 */

require_once "includes/pageLoader.php";

$pageVars = [];

// Verify if the user is logged in.
$verifyLogin = new \cst126\verifyLogin();


$addResult = false;

// Check if form submitted
if (isset($_POST["submit"])) {
	$addSubmit = new \cst126\AddBlogEntry();
	$addResult = $addSubmit->doBlogAdd();
}


// Check session vars
$chkSession = new cst126\CheckSession();
$pageVars = array_merge($pageVars, $chkSession->sessionResults);

//var_dump($pageVars);

if ($verifyLogin->verify()) {
	$pageVars["pageTitle"] = "Add Blog Entry | CST-126 Blog";
	$pageVars["pageName"] = "addBlog";

	echo $twig->render( 'addBlog.html.twig', $pageVars );
} else {
	$pageVars["pageTitle"] = "Login | CST-126 Blog";
	$pageVars["pageName"] = "login";

	echo $twig->render( 'login.html.twig', $pageVars );
}