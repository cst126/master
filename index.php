<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        7/2017
 *
 * File:        index.php
 *
 * Blog home page
 */

require_once "includes/pageLoader.php";

$pageVars = [];

// Verify if the user is logged in.
$verifyLogin = new \cst126\verifyLogin();

// Check session vars
$chkSession = new cst126\CheckSession();
$pageVars = array_merge($pageVars, $chkSession->sessionResults);


//var_dump($pageVars);

if ($verifyLogin->verify()) {
	$pageVars["pageTitle"] = "CST 126 Blog Application";
	$pageVars["pageName"]  = "index";
	$pageVars["access"] = $_SESSION["access"];

	echo $twig->render( 'index.html.twig', $pageVars );
} else {
	$pageVars["pageTitle"] = "Login | CST-126 Blog";
	$pageVars["pageName"] = "login";

	echo $twig->render( 'login.html.twig', $pageVars );
}