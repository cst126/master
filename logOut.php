<?php
/**
 * Project:     CST-126 Blog
 * Version:     1.0.0
 * Developers:  Ron Pearl, Aaron Fonseca
 * Date:        7/2017
 *
 * File:        logOut.php
 *
 * This is the logOut page for the blog.
 */

    session_start();

    if (isset($_SESSION['fName'])){unset($_SESSION['fName']);}
    if (isset($_SESSION['lName'])){unset($_SESSION['lName']);}
    if (isset($_SESSION['username'])){unset($_SESSION['username']);}
    if (isset($_SESSION['email'])){unset($_SESSION['email']);}
    if (isset($_SESSION["usernameLoggedIn"])){unset($_SESSION['usernameLoggedIn']);}
    if (isset($_SESSION["userIdLoggedIn"])){unset($_SESSION['userIdLoggedIn']);}
    if (isset($_SESSION["access"])){unset($_SESSION['access']);}

    // Get Server URL
    $host  = $_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
    header("Location: http://$host/login.php");
?>