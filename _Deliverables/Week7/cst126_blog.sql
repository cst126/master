-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 08, 2017 at 09:59 AM
-- Server version: 10.2.7-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cst126_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogEntry`
--

CREATE TABLE `blogEntry` (
  `id` smallint(4) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blogEntry`
--

INSERT INTO `blogEntry` (`id`, `title`, `content`, `created_at`) VALUES
(8, 'This is a test', 'this is a new blog', '2017-08-20 18:59:46'),
(9, 'by ctim', 'CAN ONLY BE DELETE BY CTIM OR ADMIN.', '2017-08-20 19:11:26'),
(13, 'Sample Blog Entry 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2017-09-08 08:53:32');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) UNSIGNED NOT NULL,
  `blogId` int(11) NOT NULL,
  `comment` text DEFAULT NULL,
  `added` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `searchTermByUser`
--

CREATE TABLE `searchTermByUser` (
  `userId` smallint(4) NOT NULL,
  `searchTermId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `searchTermByUser`
--

INSERT INTO `searchTermByUser` (`userId`, `searchTermId`) VALUES
(4, 6),
(4, 7);

-- --------------------------------------------------------

--
-- Table structure for table `searchTerms`
--

CREATE TABLE `searchTerms` (
  `id` int(11) NOT NULL,
  `termCount` int(11) NOT NULL,
  `term` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `searchTerms`
--

INSERT INTO `searchTerms` (`id`, `termCount`, `term`) VALUES
(6, 1, 'aa'),
(7, 1, 'tim');

-- --------------------------------------------------------

--
-- Table structure for table `userBlog`
--

CREATE TABLE `userBlog` (
  `userId` smallint(4) NOT NULL,
  `blogId` smallint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userBlog`
--

INSERT INTO `userBlog` (`userId`, `blogId`) VALUES
(1, 8),
(4, 9),
(9, 13);

-- --------------------------------------------------------

--
-- Table structure for table `userComments`
--

CREATE TABLE `userComments` (
  `userId` smallint(4) NOT NULL,
  `commentId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` smallint(4) NOT NULL,
  `fName` varchar(100) DEFAULT NULL,
  `lName` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `access` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fName`, `lName`, `username`, `email`, `password`, `access`, `created_at`) VALUES
(1, 'Aaron', 'Fonseca', 'afonseca', 'aaron.fonseca@gcu.edu', '$2y$11$rvbG3.gX7uA11A9ch/HtdOFws/aEWhj6qhgme33NvHNY9EtqL4JCq', 'admin', '2017-07-29 03:49:28'),
(2, 'Alma', 'Flynn', 'qixifoto', 'fotybobo@gmail.com', '$2y$11$vhBDxtPPO/zgCXanXGcqnO4XshDaZWAOaEKtvp.dVnzgZSJgM2Vby', 'user', '2017-07-29 04:51:37'),
(3, 'billy', 'smith', 'bsmith', 'bsmith@gmail.com', '$2y$11$weJgAiyLXvIq04rZcJBHpO2ZpuqYGUvXUH4QsoOzvIdhwAe3plB6m', 'admin', '2017-07-29 09:53:29'),
(4, 'Carson', 'Timmy', 'ctim', 'ctim@gmail.com', '$2y$11$R.LwtjcZKWgyJglBlocNUO72KBeEdm5/8MyIapPkT/cWpDiiHAB5K', 'user', '2017-07-31 03:50:45'),
(5, 'Jonas', 'Davis', 'jdavis', 'savyjixoz@hotmail.com', '$2y$11$saZYaPBYn.MHG1KFMMeHxuNuUXHzVTLu4fLAMJ4hAkwqPSQl1xtn6', 'user', '2017-08-04 23:41:40'),
(6, 'timmy', 'tester', 'tester', 'tester@gcu.edu', '$2y$11$1ehDdhM8glMMgrIRd1bBw.b70TPwfhkvPAw6/7sXIpw.1UTkz4rG.', 'user', '2017-08-08 23:58:33'),
(8, 'tester67', 'tester', 'tester67', 'tester67@gmail.com', '$2y$11$rvbG3.gX7uA11A9ch/HtdOFws/aEWhj6qhgme33NvHNY9EtqL4JCq', 'user', '2017-08-21 05:31:18'),
(9, 'Test', 'Person', 'test', 'test@test.com', '$2y$11$xeWr3CwId1lS7AUPSI0aU.E/IkYO8KGupDDsAvdIUK4AGwBmPyxsG', 'user', '2017-08-31 20:33:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogEntry`
--
ALTER TABLE `blogEntry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `searchTermByUser`
--
ALTER TABLE `searchTermByUser`
  ADD KEY `searchTermId` (`searchTermId`);

--
-- Indexes for table `searchTerms`
--
ALTER TABLE `searchTerms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `term` (`term`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blogEntry`
--
ALTER TABLE `blogEntry`
  MODIFY `id` smallint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `searchTerms`
--
ALTER TABLE `searchTerms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` smallint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
