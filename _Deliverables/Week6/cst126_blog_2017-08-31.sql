# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.22-MariaDB)
# Database: cst126_blog
# Generation Time: 2017-08-31 16:49:59 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table blogEntry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blogEntry`;

CREATE TABLE `blogEntry` (
  `id` smallint(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `blogEntry` WRITE;
/*!40000 ALTER TABLE `blogEntry` DISABLE KEYS */;

INSERT INTO `blogEntry` (`id`, `title`, `content`, `created_at`)
VALUES
	(8,'This is a test','this is a new blog','2017-08-20 18:59:46'),
	(9,'by ctim','CAN ONLY BE DELETE BY CTIM OR ADMIN.','2017-08-20 19:11:26');

/*!40000 ALTER TABLE `blogEntry` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `blogId` int(11) DEFAULT NULL,
  `userName` text,
  `comment` text,
  `added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;

INSERT INTO `comments` (`id`, `blogId`, `userName`, `comment`, `added`)
VALUES
	(1,8,'afonseca','testing blog post 8','2017-08-30 14:49:35'),
	(3,8,'tester','this is a test for blog 8.2','2017-08-30 14:54:14'),
	(4,8,'jdavis','this is a test for blog 8.2','2017-08-30 15:14:52'),
	(12,9,'tester','this is a test','2017-08-31 09:02:05'),
	(13,9,'jdavis','this is a test','2017-08-31 09:07:51'),
	(14,9,'tester','this is a test','2017-08-31 09:08:00'),
	(15,9,'afonseca','testing 45','2017-08-31 09:08:20'),
	(16,9,'tester','ziggy timmy','2017-08-31 09:25:34'),
	(17,8,'tester','Another test slkdfgjsdk skdfjsdlfk lksdjfsldkfj ksdjflsdkfj\r\nsjdflskdjf lksdjflskdjf','2017-08-31 09:49:20');

/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table searchTermByUser
# ------------------------------------------------------------

DROP TABLE IF EXISTS `searchTermByUser`;

CREATE TABLE `searchTermByUser` (
  `userId` smallint(4) NOT NULL,
  `searchTermId` int(11) NOT NULL,
  KEY `searchTermId` (`searchTermId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `searchTermByUser` WRITE;
/*!40000 ALTER TABLE `searchTermByUser` DISABLE KEYS */;

INSERT INTO `searchTermByUser` (`userId`, `searchTermId`)
VALUES
	(4,6),
	(4,7);

/*!40000 ALTER TABLE `searchTermByUser` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table searchTerms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `searchTerms`;

CREATE TABLE `searchTerms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `termCount` int(11) NOT NULL,
  `term` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `term` (`term`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `searchTerms` WRITE;
/*!40000 ALTER TABLE `searchTerms` DISABLE KEYS */;

INSERT INTO `searchTerms` (`id`, `termCount`, `term`)
VALUES
	(6,1,'aa'),
	(7,1,'tim');

/*!40000 ALTER TABLE `searchTerms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table userBlog
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userBlog`;

CREATE TABLE `userBlog` (
  `userId` smallint(4) NOT NULL,
  `blogId` smallint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `userBlog` WRITE;
/*!40000 ALTER TABLE `userBlog` DISABLE KEYS */;

INSERT INTO `userBlog` (`userId`, `blogId`)
VALUES
	(1,8),
	(4,9);

/*!40000 ALTER TABLE `userBlog` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` smallint(4) NOT NULL AUTO_INCREMENT,
  `fName` varchar(100) DEFAULT NULL,
  `lName` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `access` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `fName`, `lName`, `username`, `email`, `password`, `access`, `created_at`)
VALUES
	(1,'Aaron','Fonseca','afonseca','aaron.fonseca@gcu.edu','$2y$11$rvbG3.gX7uA11A9ch/HtdOFws/aEWhj6qhgme33NvHNY9EtqL4JCq','admin','2017-07-28 20:49:28'),
	(2,'Alma','Flynn','qixifoto','fotybobo@gmail.com','$2y$11$vhBDxtPPO/zgCXanXGcqnO4XshDaZWAOaEKtvp.dVnzgZSJgM2Vby','user','2017-07-28 21:51:37'),
	(3,'billy','smith','bsmith','bsmith@gmail.com','$2y$11$weJgAiyLXvIq04rZcJBHpO2ZpuqYGUvXUH4QsoOzvIdhwAe3plB6m','admin','2017-07-29 02:53:29'),
	(4,'Carson','Timmy','ctim','ctim@gmail.com','$2y$11$R.LwtjcZKWgyJglBlocNUO72KBeEdm5/8MyIapPkT/cWpDiiHAB5K','user','2017-07-30 20:50:45'),
	(5,'Jonas','Davis','jdavis','savyjixoz@hotmail.com','$2y$11$saZYaPBYn.MHG1KFMMeHxuNuUXHzVTLu4fLAMJ4hAkwqPSQl1xtn6','user','2017-08-04 16:41:40'),
	(6,'timmy','tester','tester','tester@gcu.edu','$2y$11$1ehDdhM8glMMgrIRd1bBw.b70TPwfhkvPAw6/7sXIpw.1UTkz4rG.','user','2017-08-08 16:58:33'),
	(8,'tester67','tester','tester67','tester67@gmail.com','$2y$11$rvbG3.gX7uA11A9ch/HtdOFws/aEWhj6qhgme33NvHNY9EtqL4JCq','user','2017-08-20 22:31:18');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
