module.exports = function(grunt) {

    require('jit-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        less: {
            production: {
                options: {
                    paths: ["css"],
                    plugins: [
                        new (require('less-plugin-autoprefix'))({browsers:["last 2 versions"]}),
                        new (require('less-plugin-clean-css'))()
                    ]
                },
                files: {
                    "css/custom.min.css" : "less/custom.less"
                }
            }
        },

        cssmin: {
            options: {
                mergeIntoShorthands: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    'css/main.min.css': [
                        'node_modules/bootstrap/dist/css/bootstrap.min.css',
                        'css/custom.min.css'
                    ]
                }
            }
        },

        concat: {
            options: {
                separator: grunt.util.linefeed + grunt.util.linefeed + ';' + grunt.util.linefeed + grunt.util.linefeed
            },
            main: {
                src: [
                    'node_modules/jquery/dist/jquery.min.js',
                    'node_modules/bootstrap/dist/js/bootstrap.min.js',
                    'js/custom.js'
                ],
                dest: 'js/main.min.js'
            }
        }

    });

    grunt.registerTask('default', ['less', 'cssmin', 'concat']);
};